import React, { Component } from "react";
import {Card, CardImg, CardImgOverlay, CardTitle, CardBody, CardText } from "reactstrap";

class Menu extends Component {
    constructor(props)
    {
        super(props);
        this.state={  
            platoSeleccionado:null
                    }
    }
    onPlatoSeleccionado(plato){
        this.setState({platoSeleccionado:plato});
    }
    renderPlato(plato){
        if(plato != null){
            return(
                <Card>
                    <CardImg top src={plato.image} alt={plato.name}/>
                    <CardBody>
                        <CardTitle>{plato.name}</CardTitle>
                        <CardText>{plato.description}</CardText>
                    </CardBody>

                </Card>
            );

        } 
        else
            return ( <div></div>);
    }
    render(){
        const menu= this.props.platos.map((plato) => {
            return(
                <div key={plato.id} className="col-12 col-md-5 mt-5">
                <Card key={plato.id} onClick={()=> this.onPlatoSeleccionado(plato)}>
                        <CardImg width="100%" src={plato.image} alt={plato.name}/>
                        <CardImgOverlay body className="ml-5">
                        <CardTitle>{plato.name} </CardTitle>
                    </CardImgOverlay>
                </Card>
                </div>
            );
        });
        return(
            <div className="container">
                <div className="row">
                        {menu}
                </div>
                <div className="row">
                        <div className="col-12 col-md-5 m-1">
                            {this.renderPlato(this.state.platoSeleccionado)}
                        </div>
                </div>
            </div>
        );
    }
}
export default Menu;